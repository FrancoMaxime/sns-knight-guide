# 2024-01-04, Version 5.2.0 (Current)

**Notable Changes**

* creating a new Envy Leveling Video, to provide a more realistical scenario








# SNS Knight 1h 1on1 Current State of Game
December 26, 2023

Utilizing the 1-on-1 EXP boost for monsters above level 141, I recommend leveling in Envy until 160. Ensure you have all DPS premiums, def upcuts, and electric def upcuts.

After trying the one-hour Envy leveling, reverting to open-world leveling won't be an option. The gained experience is too remarkable to settle for regular leveling.



# **Pets**
Your favored companion would undoubtedly be the Critrate Pet.
Gradually leveling up a pet is achievable as you grind.

In my view, a critrate pet within the range of 15 - 20% should suffice.
Given the current expense of pets, it is advisable to upgrade your pet in a measured manner.


# **1h Knight SNS Build**
## **Skills**
### **Heart of Fury**
To provide a broad overview of its functioning:
10% of your overall FP transforms into supplementary damage.
With this understanding, it becomes evident that the FP-Necklace is indispensable in any scenario.

![Heart of Fury](hof.jpg)

Which means in the following:

Imagine we got a fresh lv 60 Knight....
Recorn set clean and **only** FP-Necklace +6 with lv. 57 Sparkling Sword unbuffed

if you go for 1on1 Leveling

| PURE STA  | PURE STR | 
| ------------- |:-------------:|
| HP: 6603    | HP: 3346    |
| FP: 2250    | FP: 1011    |
| ATK: **479**      | ATK: **1010**   |
| STR: 18     | STR: 136    |
| STA: 133    | STA: 15   |
| HOF:  **225** (10% of 2250)     | HoF: **101** (10% of 1011)   |


As evident, a PURE STR build (1010 ATK + 101, approximately equivalent to Demol +10) deals significantly more damage than a PURE STA build (479 ATK + 225, around a Demol +13). Despite the additional damage on paper from HoF with PURE STA, the base ATK of PURE STR is inherently stronger. With 1h scaling from STR, attacks become substantially more potent, particularly as scaling synergizes with crit rate and crit damage. The PURE STR build stands out as a superior choice for maximizing EXP % per hour.



For pure STA Knights, the Heart of Fury (HoF) serves as a beneficial tool for engaging in battles against giants, bosses, and superbosses such as CW and Meteonyker.

### **Rage**
![Rage](rage.jpg)


* **Attack +15%**
* **Incomming Damage +10%**
* **Increased -10%HP**
* **Block Penentration +15%**

An effective damage-dealing skill.
This skill is crucial for 1-on-1 encounters.


#**Gear**
## **Low Budget Build**
For the easiest starter build you can go is


* **Dretra +0 / Sparkling Sword +0**
* **Recorn Set +0 / Pangril +0**
* **Demol +6 / Vigor +6 / FP-Necklace +7 / Enduky +1**

I discourage opting for any critical build because it heavily depends on a consistently fast autoattack. This advice takes into account the buffs from FS/RM with only 50 INT.
You should get these following stats:

| Full STR  |
| ------------- |
| HP: 4452    | 
| FP: 1284   | 
| ATK: **1771**      | 
| STR: 173     | 
| STA: 41    | 
| Atkspd: 92    | 
| HOF:  **192** (15% of 1284)     | 


Based on HoF scaling, you should achieve approximately ~2k damage per autoattack. To make up for the lacking 8% Attack Speed, it's more efficient to utilize the Potion of Swiftness (a 10% attack speed potion). If you're averse to using attack speed potions, allocate points into Dexterity until you reach 100% Hitrate when buffed, although you may need to respec later. My preference leans towards maximizing damage.


## **High Budget Build**

With a investment you can build:

* **Dretra +8 - +10 / Historic Sword 1h +8 - +10**
* **(Lv. 30) Comet Set +8 16% ATK**
* **Demol +10 / Vigor +10 / FP-Necklace +7 / Enduky +1 / +2**
* **Critrate Pet**

you can similar expect to do this DMG:

https://youtu.be/jPs77KaR2Eg

this is just a session of me leveling at my 2nd SNS character 



# **Leveling Guide**
## **1-60**

One of the most expeditious methods I have identified involves consistently utilizing the skill "Slash," accessible at level 15. This approach requires the availability of an FP refresher and basic equipment, even including NPC items. Optimal results can be achieved with any high base ATK weapon, irrespective of whether it is an Axe or Sword, emphasizing a PURE STR build. For individuals equipped with the Comet Set at levels 30, ranging from +6 to +8, employing autoattacks is advisable.


## **60 - 74**

Following the job change, it is advisable to maximize both self-buff skills for the Sword, namely **Empowered Weapon**, **Blazing Sword**, and **Sword Mastery**. Despite the Sword's faster animation, the Axe possesses superior damage potential, and the choice between them is subjective. Personally, I favor the Sword.

Upon reaching level 60, a pivotal skill called **Heart of Fury** becomes available, with each attack consuming a variable amount depending on the chosen build. As of July 21, 2022, in a full STR build, using an FP refresher once every 10 minutes is recommended—a less arduous approach **compared to previous buffs**. 

Notably, farming Carrier Bombs from levels 72 to 77 is highly recommended. This yields 6 million or more per hour solely from NPC drops, thanks to the 1-on-1 buffs providing 30% experience and 20% loot increase. (I plan to share a YouTube video on January 25, 2023.) This calculation does not include dice or 4% cards. Additionally, creating your own party and setting it to level can be advantageous, keeping your RM at a similar level. Although the grinding process may be time-consuming, the rewards are worthwhile, making it easier to obtain the Historic Sword and Wees/Wesshian Set. **The level curve is notably steady**.


## **LV 75**

Upon reaching this level, it is advisable to incorporate the use of the Historic Sword. I strongly recommend utilizing a well-upgraded variant for optimal performance. Concurrently, you can commence the search for the Meteo accessory known as Enducky.

In the present context, an Enducky+1 suffices; acquiring a +3 version provides only a marginal increase of around 20 additional damage, making it economically impractical. 
The price is contingent on each server, with well-established servers potentially offering more affordable rates compared to newly launched servers. 


The level curve remains consistently stable.


## **LV 90**

At this point, it is advisable to equip the Wees / Weeshian Set. I recommend upgrading it to +6; going beyond +8 is not deemed cost-effective.

If you have surplus penya, consider upgrading your Demols and Vigors, aiming for an average of around +9/10.

Regarding the level curve, there is a noticeable decline starting at level 90, signifying an increased time requirement to defeat monsters, resulting in a more demanding grind until reaching level 105.

Updated Level-curve Information (as of January 25, 2023):
The level curve remains solid, attributed to the 1-on-1 experience buff introduced on January 25, 2023.

## **LV 105**

Presently, our ultimate end-game equipment consists of the Legendary Golden Sword 1h and the Ectro/Extro Set.

I strongly recommend focusing on penya farming in Glaphan, particularly between levels 99 and 105. In this area, you can achieve an approximate income of ~10 million penya per hour from NPC loot, a practice familiar to you from your earlier farming at level 72.

Updated Level-curve Information (as of January 25, 2023):
The level curve remains stable, thanks to the implementation of the 1-on-1 experience buff on January 25, 2023.

## **Lv 110/111+**


Commencing at either level 110 or 111, consider transitioning to Azria and undertake leveling on Medument, persisting until reaching level 120. Subsequently, upon reaching this threshold, explore the acquisition of a clean Bloody Sword and proceed to upgrade it to +8. The cost of obtaining such a weapon varies, contingent upon the specific server you are on, ranging from potentially expensive to comparatively affordable, predicated on the penya accrued from your preceding farming endeavors in Glaphan.

Upon attaining level 120, redirect your leveling efforts to Augu until reaching level 125. Beyond this point, advance to Ghosts and continue leveling until reaching level 132. Starting from level 133, shift your focus to Mammoths and persist in leveling until reaching level 137.

While progressing towards level 137, it is advisable to initiate the upgrading process for your equipment, transitioning to the 135 set. Aim to upgrade this set to at least +8, incorporating enhancements such as 16/28% Attack.

Simultaneously, consider advancing the upgrading of your jewels to their maximum potential. In my experience, achieving a +18 enhancement for the jewels becomes feasible at this stage.

Upon reaching level 137, commence your leveling endeavors on Coral Island, beginning with Atrox encounters at level 141 and gradually progressing to Tigar. During this phase, it is prudent to contemplate farming smaller Tigar creatures to amass sufficient penya. This acquired penya can then be strategically utilized to upgrade your equipment, including the set to +10, the weapon to +10, or enhancing jewelry to levels +19/+20.

## 
# Pets

I highly advise acquiring a Critical Damage (Crit-DMG) pet once your Critrate surpasses 70%—this is beneficial both for leveling and engaging in CW or Meteor fights, where a Crit-Rate Pet is particularly advantageous.

# Red Meteonyker Video

PURE STR
https://youtu.be/c40GTB_0k-M

PURE STA
https://www.youtube.com/watch?v=ZjeDWQsWhqA

general rule: if you want to solo farm CW/ Red meteor allways go pure sta as of current update you lose less than 1% DPS
super comfort runs, u only die on melts, so just comfortable run away while having 30k+ HP 

pure str suffering more than 10% DPS nerf


# The current final meta Build 

This prevailing meta build is contingent on possessing the latest jewelry set, requiring specific components:

FWC (140 Set, FWC 140 1h Weapon) OR 135 Set, Bloody Sword

Champions Set, either at +0 or +4 (important to note FP Necklace)


The rationale behind employing Sword and Shield (SnS) lies in the meticulous consideration of the absolute min-max factor. SnS demonstrates superior DPS compared to a 2h weapon. The disparity arises from the 2h weapon's dependence on the swordcross with a 22% chance to proc, thus diminishing its overall DPS. Despite the raw strength of 2h weapons and str scaling on axe or sword, the reduction caused by the approximately 22% factor underscores its comparative weakness.

The proposed optimal balancing for SnS Knight and 2h Knight suggests that, under a full DPS build, they should achieve comparable DPS. This approach fosters a balanced environment, enabling the community to explore diverse playstyles instead of adhering to a singular meta build.

It is emphasized that employing a 2h weapon as a DPS choice while playing as a knight is deemed counterproductive to the party, given its substantially lower DPS (around 200k) compared to an SnS's 400-500k DPS. This sentiment is familiar to those who have collaborated in FWC.

# Envy Leveling 
-- creating a new one


> **This Guide is for any SNS lover.**

>
>> **This Guide is written in Markdown.**




